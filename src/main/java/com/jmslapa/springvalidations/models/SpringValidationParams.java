package com.jmslapa.springvalidations.models;

import lombok.*;

@RequiredArgsConstructor
@Getter
@Builder
public class SpringValidationParams {
    private final String defaultDateFormat;
}
