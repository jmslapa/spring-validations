package com.jmslapa.springvalidations.configuration;

import com.jmslapa.springvalidations.models.SpringValidationParams;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.jmslapa.springvalidations"})
public class SpringValidationsConfiguration {

    protected SpringValidationParams buildSpringValidationsConfigBean() {
        return SpringValidationParams.builder()
                .defaultDateFormat("yyyy-MM-dd")
                .build();
    }

    @Bean
    public SpringValidationParams springValidationsConfigsBean() {
        return buildSpringValidationsConfigBean();
    }

}
