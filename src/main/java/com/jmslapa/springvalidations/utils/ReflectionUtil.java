package com.jmslapa.springvalidations.utils;

import java.lang.reflect.*;

@SuppressWarnings("java:S3011")
public class ReflectionUtil {

    private ReflectionUtil() {
    }

    public static Field getField(Object subject, String fieldName) throws NoSuchFieldException {
        try {
            return subject.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException noSuchFieldException) {
            try {
                return subject.getClass().getSuperclass().getDeclaredField(fieldName);
            } catch (NoSuchFieldException noSuchSuperClassFieldException) {
                throw (NoSuchFieldException) noSuchSuperClassFieldException.initCause(noSuchFieldException);
            }
        }
    }

    public static Object getFieldValue(Object subject, String fieldName)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = getField(subject, fieldName);
        field.setAccessible(true);
        return field.get(subject);
    }

    public static Method getMethod(Class<?> clazz, String method, Class<?>... argTypes) throws NoSuchMethodException {
        try {
            return clazz.getDeclaredMethod(method, argTypes);
        } catch (NoSuchMethodException noSuchMethodException) {
            try {
                return clazz.getSuperclass().getDeclaredMethod(method, argTypes);
            } catch (NoSuchMethodException noSuchSuperClassMethodException) {
                throw (NoSuchMethodException) noSuchSuperClassMethodException.initCause(noSuchMethodException);
            }
        }
    }

    public static Object invokeMethod(Object accessor, String methodName, Class<?>[] argTypes, Object[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = getMethod(accessor.getClass(), methodName, argTypes);
        return method.invoke(Modifier.isStatic(method.getModifiers()) ? null : accessor, args);
    }
}
