package com.jmslapa.springvalidations.utils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQueries;
import java.util.Optional;

public class TemporalUtil {

    protected TemporalUtil() {
    }

    public static Instant toInstant(TemporalAccessor temporalAccessor) {
        return LocalDateTime.of(
                Optional.ofNullable(temporalAccessor.query(TemporalQueries.localDate())).orElse(LocalDate.ofEpochDay(0)),
                Optional.ofNullable(temporalAccessor.query(TemporalQueries.localTime())).orElse(LocalTime.ofSecondOfDay(0))
        ).toInstant(Optional.ofNullable(temporalAccessor.query(TemporalQueries.offset())).orElse(ZoneOffset.UTC));
    }

    public static TemporalAccessor resolveByFormat(String dateString, DateTimeFormatter formatter) {
        try {
            return LocalDateTime.parse(dateString, formatter);
        } catch (DateTimeParseException dateTimeParseException) {
            try {
                return LocalDate.parse(dateString, formatter);
            } catch (DateTimeParseException dateParseException) {
                try {
                    return LocalTime.parse(dateString, formatter);
                } catch (DateTimeParseException timeParseException) {
                    throw new DateTimeException("Can't resolve type by given format and date string.");
                }
            }
        }
    }
}
