package com.jmslapa.springvalidations.utils;

import com.jmslapa.springvalidations.exceptions.AnnotationExtractionException;
import lombok.extern.log4j.Log4j2;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

@Log4j2
public class AnnotationExtractor {

    private AnnotationExtractor() {
    }

    public static <T extends Annotation> T getAnnotation(Object subject,
                                                         String fieldName,
                                                         Class<? extends T> annotationClass) {
        try {
            Field field = ReflectionUtil.getField(subject, fieldName);
            if (!field.isAnnotationPresent(annotationClass)) {
                throw makeException(String.format(
                        "Mapping error: the field %s of class %s is not decorated with DateTimeFormat annotation.",
                        fieldName,
                        subject.getClass().getName()
                ));
            }
            return annotationClass.cast(field.getAnnotation(annotationClass));
        } catch (NoSuchFieldException e) {
            throw makeException(String.format(
                    "Mapping error: was not possible find the field '%s' in the class '%s'.",
                    fieldName,
                    subject.getClass().getName()
            ));
        }
    }

    private static AnnotationExtractionException makeException(String message) {
        log.error(message);
        return new AnnotationExtractionException("Whoops! An object mapping error has occurred.");
    }
}
