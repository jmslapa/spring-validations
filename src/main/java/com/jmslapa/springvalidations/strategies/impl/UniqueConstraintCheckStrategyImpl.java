package com.jmslapa.springvalidations.strategies.impl;

import com.jmslapa.springvalidations.exceptions.UniqueConstraintValidationException;
import com.jmslapa.springvalidations.strategies.UniqueConstraintCheckStrategy;
import com.jmslapa.springvalidations.utils.ReflectionUtil;

import java.util.Optional;

public abstract class UniqueConstraintCheckStrategyImpl<T> implements UniqueConstraintCheckStrategy {
    @Override
    public boolean isValid(Object object, String[] fields) {

        boolean isValid = true;
        for (String field : fields) {
            try {
                Optional<T> user = findSubjectByField(field, ReflectionUtil.getFieldValue(object, field));
                if (user.isPresent()) {
                    isValid = false;
                }
            } catch (NoSuchFieldException e) {
                throw new UniqueConstraintValidationException(String.format("Field '%s' not found.", field), e);
            } catch (IllegalAccessException e) {
                throw new UniqueConstraintValidationException(e.getMessage(), e);
            }
        }
        return isValid;
    }

    @Override
    public boolean isValid(Object object, String[] fields, String except) {
        boolean isValid = true;
        for (String field : fields) {
            try {
                Optional<T> opt = findSubjectByField(field, ReflectionUtil.getFieldValue(object, field));
                if (opt.isPresent() && !isSameSubject(object, opt.get(), except)) {
                    isValid = false;
                }
            } catch (NoSuchFieldException e) {
                throw new UniqueConstraintValidationException(String.format("Field '%s' not found.", field), e);
            } catch (IllegalAccessException e) {
                throw new UniqueConstraintValidationException(e.getMessage(), e);
            }
        }
        return isValid;
    }

    protected boolean isSameSubject(Object object, T subject, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Object objFieldValue = ReflectionUtil.getFieldValue(object, fieldName);
        Object subjectFieldValue = ReflectionUtil.getFieldValue(subject, fieldName);
        return objFieldValue.equals(subjectFieldValue);
    }

    protected abstract Optional<T> findSubjectByField(String field, Object value);
}
