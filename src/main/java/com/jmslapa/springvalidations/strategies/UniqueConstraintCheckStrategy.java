package com.jmslapa.springvalidations.strategies;

public interface UniqueConstraintCheckStrategy {
    boolean isValid(Object object, String[] fields);
    boolean isValid(Object object, String[] fields, String except);
}
