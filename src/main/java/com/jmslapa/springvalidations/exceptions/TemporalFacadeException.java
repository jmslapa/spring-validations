package com.jmslapa.springvalidations.exceptions;

public class TemporalFacadeException extends RuntimeException {
    public TemporalFacadeException() {
        super();
    }

    public TemporalFacadeException(String message) {
        super(message);
    }

    public TemporalFacadeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TemporalFacadeException(Throwable cause) {
        super(cause);
    }

    protected TemporalFacadeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
