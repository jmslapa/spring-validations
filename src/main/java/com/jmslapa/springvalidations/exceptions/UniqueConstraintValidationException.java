package com.jmslapa.springvalidations.exceptions;

public class UniqueConstraintValidationException extends RuntimeException {
    public UniqueConstraintValidationException() {
        super();
    }

    public UniqueConstraintValidationException(String message) {
        super(message);
    }

    public UniqueConstraintValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UniqueConstraintValidationException(Throwable cause) {
        super(cause);
    }

    protected UniqueConstraintValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
