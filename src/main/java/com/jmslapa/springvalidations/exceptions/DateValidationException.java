package com.jmslapa.springvalidations.exceptions;

public class DateValidationException extends RuntimeException {
    public DateValidationException() {
        super();
    }

    public DateValidationException(String message) {
        super(message);
    }

    public DateValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DateValidationException(Throwable cause) {
        super(cause);
    }

    protected DateValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
