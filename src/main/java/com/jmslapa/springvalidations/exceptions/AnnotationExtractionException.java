package com.jmslapa.springvalidations.exceptions;

public class AnnotationExtractionException extends RuntimeException {

    public AnnotationExtractionException() {
    }

    public AnnotationExtractionException(String message) {
        super(message);
    }

    public AnnotationExtractionException(String message, Throwable cause) {
        super(message, cause);
    }

    public AnnotationExtractionException(Throwable cause) {
        super(cause);
    }

    protected AnnotationExtractionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
