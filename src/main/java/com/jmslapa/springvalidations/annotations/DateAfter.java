package com.jmslapa.springvalidations.annotations;

import com.jmslapa.springvalidations.validators.DateAfterValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateAfterValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateAfter {
    String message() default "com.jmslapa.springvalidations.annotations.DateAfter.message";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String value() default "now";

    String format() default "";
}
