package com.jmslapa.springvalidations.annotations;

import com.jmslapa.springvalidations.strategies.UniqueConstraintCheckStrategy;
import com.jmslapa.springvalidations.validators.UniqueValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Unique {
    String message() default "com.jmslapa.springvalidations.annotations.Unique.message";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Class<? extends UniqueConstraintCheckStrategy> checkStrategy();

    String qualifier() default "";

    String[] fields();

    String except() default "";
}

