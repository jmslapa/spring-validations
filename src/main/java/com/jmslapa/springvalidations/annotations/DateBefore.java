package com.jmslapa.springvalidations.annotations;

import com.jmslapa.springvalidations.validators.DateBeforeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateBeforeValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateBefore {
    String message() default "com.jmslapa.springvalidations.annotations.DateBefore.message";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String value() default "now";

    String format() default "";
}
