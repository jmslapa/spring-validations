package com.jmslapa.springvalidations.validators;

import com.jmslapa.springvalidations.exceptions.DateValidationException;
import com.jmslapa.springvalidations.exceptions.TemporalFacadeException;
import com.jmslapa.springvalidations.facades.TemporalFacade;
import lombok.extern.log4j.Log4j2;

import javax.validation.ConstraintValidator;
import java.lang.annotation.Annotation;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.function.Predicate;

@Log4j2
public abstract class DateMomentValidator <A extends Annotation> implements ConstraintValidator<A, String> {
    protected String format;
    protected DateTimeFormatter formatter;
    protected String moment;

    @Override
    public void initialize(A constraintAnnotation) {
        format = getFormat(constraintAnnotation);
        moment = getMoment(constraintAnnotation);
        formatter = DateTimeFormatter.ofPattern(format);
    }

    protected abstract String getMoment(A constraintAnnotation);

    protected abstract String getFormat(A constraintAnnotation);

    protected abstract boolean hasValidFormat(String dateString);

    protected TemporalFacade parseDate(String dateString, TemporalAccessor accessor) {
        TemporalFacade parsedDate;
        try {
            parsedDate = dateString.equals("now")
                    ? TemporalFacade.now(accessor)
                    : TemporalFacade.createBySentence(dateString, accessor);
        } catch (TemporalFacadeException e) {
            if (!hasValidFormat(dateString)) {
                throw new DateValidationException(String.format(
                        "The 'before' argument must have the %s format or be a valid temporal sentence.", format));
            }
            parsedDate = TemporalFacade.parse(accessor, dateString, formatter);
        }
        return parsedDate;
    }
}
