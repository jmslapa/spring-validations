package com.jmslapa.springvalidations.validators;

import com.jmslapa.springvalidations.annotations.DateBefore;
import com.jmslapa.springvalidations.facades.TemporalFacade;
import com.jmslapa.springvalidations.models.SpringValidationParams;
import com.jmslapa.springvalidations.utils.TemporalUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;
import java.time.DateTimeException;
import java.time.temporal.TemporalAccessor;

@Log4j2
@Component
@RequiredArgsConstructor
public class DateBeforeValidator extends DateMomentValidator<DateBefore> {
    private final SpringValidationParams springValidationParams;
    private TemporalFacade parsedDateString;

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        try {
            TemporalAccessor accessor = TemporalUtil.resolveByFormat(s, formatter);
            parsedDateString = TemporalFacade.parse(accessor, s, formatter);
            return moment.isEmpty() || parsedDateString.isBefore(parseDate(moment, accessor));
        } catch (DateTimeException e) {
            return false;
        }
    }

    @Override
    protected String getMoment(DateBefore constraintAnnotation) {
        return constraintAnnotation.value();
    }

    @Override
    protected String getFormat(DateBefore constraintAnnotation) {
        return constraintAnnotation.format().isEmpty()
                ? springValidationParams.getDefaultDateFormat()
                : constraintAnnotation.format();
    }

    @Override
    protected boolean hasValidFormat(String dateString) {
        return parsedDateString.format(formatter).equals(dateString);
    }
}
