package com.jmslapa.springvalidations.validators;

import com.jmslapa.springvalidations.annotations.DateFormat;
import com.jmslapa.springvalidations.facades.TemporalFacade;
import com.jmslapa.springvalidations.models.SpringValidationParams;
import com.jmslapa.springvalidations.utils.TemporalUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DateTimeException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

@Log4j2
@Component
@RequiredArgsConstructor
public class DateFormatValidator implements ConstraintValidator<DateFormat, String> {
    private final SpringValidationParams springValidationParams;
    private DateTimeFormatter formatter;

    protected String getFormat(DateFormat constraintAnnotation) {
        return constraintAnnotation.value().isEmpty()
                ? springValidationParams.getDefaultDateFormat()
                : constraintAnnotation.value();
    }

    @Override
    public void initialize(DateFormat constraintAnnotation) {
        formatter = DateTimeFormatter.ofPattern(getFormat(constraintAnnotation));
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        try {
            TemporalAccessor accessor = TemporalUtil.resolveByFormat(s, formatter);
            return TemporalFacade.parse(accessor, s, formatter).format(formatter).equals(s);
        } catch (DateTimeException e) {
            return false;
        }
    }
}
