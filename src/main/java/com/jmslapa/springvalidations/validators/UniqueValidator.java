package com.jmslapa.springvalidations.validators;

import com.jmslapa.springvalidations.annotations.Unique;
import com.jmslapa.springvalidations.providers.ContextProvider;
import com.jmslapa.springvalidations.strategies.UniqueConstraintCheckStrategy;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueValidator implements ConstraintValidator<Unique, Object> {
    private UniqueConstraintCheckStrategy checkStrategy;
    private String[] fields;
    private String except;

    @Override
    public void initialize(Unique unique) {
        Class<? extends UniqueConstraintCheckStrategy> checkStrategyClass = unique.checkStrategy();
        String qualifier = unique.qualifier();
        if (qualifier.isEmpty()) {
            this.checkStrategy = ContextProvider.getBean(checkStrategyClass);
        } else {
            this.checkStrategy = ContextProvider.getBean(qualifier, checkStrategyClass);
        }
        this.fields = unique.fields();
        this.except = unique.except();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        return except.isEmpty() ? checkStrategy.isValid(object, fields) : checkStrategy.isValid(object, fields, except);
    }
}
