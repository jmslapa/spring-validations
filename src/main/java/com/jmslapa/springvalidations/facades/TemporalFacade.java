package com.jmslapa.springvalidations.facades;

import com.jmslapa.springvalidations.enums.TimeMoment;
import com.jmslapa.springvalidations.exceptions.TemporalFacadeException;
import com.jmslapa.springvalidations.utils.ReflectionUtil;
import com.jmslapa.springvalidations.utils.TemporalUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQueries;
import java.time.temporal.TemporalUnit;
import java.util.Date;

public class TemporalFacade {
    private final TemporalAccessor accessor;

    public TemporalFacade(TemporalAccessor accessor) {
        validateInstance(accessor);
        this.accessor = accessor;
    }

    private TemporalAccessor getAccessor() {
        return this.accessor;
    }

    private static <T extends TemporalAccessor> Object invokeMethod(T accessor, String methodName, Class<?>[] argTypes, Object[] args) {
        try {
            return ReflectionUtil.invokeMethod(accessor, methodName, argTypes, args);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new TemporalFacadeException("Method invocation error", e);
        }
    }

    private static <T extends TemporalAccessor> Object invokeMethod(T accessor, String methodName) {
        Class<?>[] argTypes = {};
        Object[] args = {};
        return invokeMethod(accessor, methodName, argTypes, args);
    }

    public static TemporalFacade now(TemporalAccessor accessor) {
        return new TemporalFacade((TemporalAccessor) invokeMethod(accessor, "now"));
    }

    public static TemporalFacade parse(TemporalAccessor accessor, String dateString, DateTimeFormatter formatter) {
        Class<?>[] argTypes = {CharSequence.class, DateTimeFormatter.class};
        Object[] args = {dateString, formatter};
        return new TemporalFacade((TemporalAccessor) invokeMethod(accessor, "parse", argTypes, args));
    }

    /**
     * @param sentence is a temporal sentence
     * @param accessor is a instance of required temporal subtype
     * @return an instance of temporal facade
     * @throws TemporalFacadeException if the sentence is malformed
     */
    public static TemporalFacade createBySentence(String sentence, TemporalAccessor accessor) {
        try {
            TemporalFacade now = TemporalFacade.now(accessor);
            String[] split = sentence.split("\\s");
            if(split.length < 3) {
                throw new IllegalArgumentException();
            }
            long amount = Long.parseLong(split[0]);
            ChronoUnit chronoUnit = ChronoUnit.valueOf(split[1].toUpperCase());
            TimeMoment timeMoment = TimeMoment.valueOf(split[2].toUpperCase());
            return timeMoment.equals(TimeMoment.AFTER) ? now.plus(amount, chronoUnit) : now.minus(amount, chronoUnit);
        } catch (IllegalArgumentException e) {
            throw new TemporalFacadeException("The temporal sentence is malformed. " +
                    "The sentence must have the format: [<amount> <time unit> <after|ago>]");
        }
    }

    public void validateInstance(TemporalAccessor accessor) {
        if (!(accessor instanceof LocalDateTime) && !(accessor instanceof LocalDate) && !(accessor instanceof LocalTime)) {
            throw new TemporalFacadeException("The accessor must be an instance of LocalDateTime, LocalDate or LocalTime.");
        }
    }

    public String format(DateTimeFormatter formatter) {
        Class<?>[] argTypes = {DateTimeFormatter.class};
        Object[] args = {formatter};
        return (String) invokeMethod(accessor, "format", argTypes, args);
    }

    public boolean isBefore(TemporalFacade temporalFacade) {
        return TemporalUtil.toInstant(accessor).isBefore(TemporalUtil.toInstant(temporalFacade.getAccessor()));
    }

    public boolean isAfter(TemporalFacade temporalFacade) {
        return TemporalUtil.toInstant(accessor).isAfter(TemporalUtil.toInstant(temporalFacade.getAccessor()));
    }

    public TemporalFacade plus(long amount, TemporalUnit temporalUnit) {
        Class<?>[] argTypes = {long.class, TemporalUnit.class};
        Object[] args = {amount, temporalUnit};
        return new TemporalFacade((TemporalAccessor) invokeMethod(accessor, "plus", argTypes, args));
    }

    private TemporalFacade minus(long amount, TemporalUnit temporalUnit) {
        Class<?>[] argTypes = {long.class, TemporalUnit.class};
        Object[] args = {amount, temporalUnit};
        return new TemporalFacade((TemporalAccessor) invokeMethod(accessor, "minus", argTypes, args));
    }
}
